from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from central_supplies.models import Product
from .cart import Cart
from .forms import CartAddProductForm
from django.contrib import messages

# Create your views here.
@require_POST
def cart_add(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        if product.stock < cd['quantity']:
            messages.error(request, f'{product} not added to cart. \
                    Quantity requested {cd["quantity"]} is more than the stock available \
                    {product.stock}.')
        else:
            cart.add(product=product,
                     quantity=cd['quantity'],
                     update_quantity=cd['update'])
    return redirect('cart:cart-detail')

def cart_remove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('cart:cart-detail')

def cart_detail(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(
                initial={'quantity': item['quantity'],
                    'update': True})
    return render(request, 'cart/detail.html', {'title': 'Your Shopping Cart', 'cart': cart})


